import os
import glob
from zipfile import ZipFile

def extract(full_filename):
    #print("full_filename: ", full_filename)
    # Step 1: Search all files with .zip.xxx
    search_files = full_filename.replace(".zip.001", ".zip")
    search_files = glob.escape(search_files) + "*" # to handle special escape characters like [. * should be seperate.
    read_files = glob.glob(search_files)
    #print(read_files)  # ['C:/path\\aaa.zip.001', 'C:/path\\aaa.zip.002', 'C:/path\\aaa.zip.003']


    # Step 2: Create output folder if not already available
    output_directory, short_filename = os.path.split(full_filename)
    output_folder = output_directory + "/" + short_filename[:-4] + "_out" # e.g. C:/path/to/aaa.zip.out
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    #print("output_folder:", output_folder)

    # Step 3: Combine all the files into 1 zip file in the new folder
    new_zipfile = output_folder + "/" + short_filename[:-4] + ".zip"
    with open(new_zipfile, "wb") as outfile:
        for f in read_files:
            with open(f, "rb") as infile:
                outfile.write(infile.read())

    # Step 4: Start unzip
    with ZipFile(new_zipfile, "r") as zipObj:
        # Ref: https://appdividend.com/2020/01/31/python-unzip-how-to-extract-single-or-multiple-files/
        # Extract all the contents of zip file in different directory
        zipObj.extractall(output_folder)

    return output_folder