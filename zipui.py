# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'zipui.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_mainWindow(object):
    def setupUi(self, mainWindow):
        mainWindow.setObjectName("mainWindow")
        mainWindow.resize(903, 397)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(mainWindow.sizePolicy().hasHeightForWidth())
        mainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QtWidgets.QWidget(mainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(20, 90, 411, 121))
        self.groupBox.setObjectName("groupBox")
        self.btnChoose = QtWidgets.QPushButton(self.groupBox)
        self.btnChoose.setGeometry(QtCore.QRect(20, 30, 75, 23))
        self.btnChoose.setObjectName("btnChoose")
        self.editFileChosen = QtWidgets.QLineEdit(self.groupBox)
        self.editFileChosen.setEnabled(True)
        self.editFileChosen.setGeometry(QtCore.QRect(120, 30, 261, 21))
        self.editFileChosen.setFocusPolicy(QtCore.Qt.NoFocus)
        self.editFileChosen.setAutoFillBackground(True)
        self.editFileChosen.setFrame(False)
        self.editFileChosen.setReadOnly(True)
        self.editFileChosen.setObjectName("editFileChosen")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(20, 60, 91, 20))
        self.label.setObjectName("label")
        self.boxFileSize = QtWidgets.QSpinBox(self.groupBox)
        self.boxFileSize.setGeometry(QtCore.QRect(120, 60, 42, 22))
        self.boxFileSize.setMinimum(1)
        self.boxFileSize.setProperty("value", 10)
        self.boxFileSize.setObjectName("boxFileSize")
        self.btnSplit = QtWidgets.QPushButton(self.groupBox)
        self.btnSplit.setEnabled(False)
        self.btnSplit.setGeometry(QtCore.QRect(20, 90, 361, 21))
        self.btnSplit.setObjectName("btnSplit")
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(20, 230, 411, 121))
        self.groupBox_2.setObjectName("groupBox_2")
        self.editStatus = QtWidgets.QTextEdit(self.groupBox_2)
        self.editStatus.setGeometry(QtCore.QRect(20, 20, 361, 91))
        self.editStatus.setObjectName("editStatus")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(30, 10, 351, 51))
        self.label_3.setLineWidth(1)
        self.label_3.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(480, 10, 371, 91))
        self.label_4.setLineWidth(1)
        self.label_4.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label_4.setObjectName("label_4")
        self.groupBox_3 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_3.setGeometry(QtCore.QRect(470, 120, 411, 91))
        self.groupBox_3.setObjectName("groupBox_3")
        self.btnChoose_2 = QtWidgets.QPushButton(self.groupBox_3)
        self.btnChoose_2.setGeometry(QtCore.QRect(20, 30, 75, 23))
        self.btnChoose_2.setObjectName("btnChoose_2")
        self.editFileChosen_2 = QtWidgets.QLineEdit(self.groupBox_3)
        self.editFileChosen_2.setEnabled(True)
        self.editFileChosen_2.setGeometry(QtCore.QRect(120, 30, 261, 21))
        self.editFileChosen_2.setFocusPolicy(QtCore.Qt.NoFocus)
        self.editFileChosen_2.setAutoFillBackground(True)
        self.editFileChosen_2.setFrame(False)
        self.editFileChosen_2.setReadOnly(True)
        self.editFileChosen_2.setObjectName("editFileChosen_2")
        self.btnUnzip = QtWidgets.QPushButton(self.groupBox_3)
        self.btnUnzip.setEnabled(False)
        self.btnUnzip.setGeometry(QtCore.QRect(20, 60, 361, 21))
        self.btnUnzip.setObjectName("btnUnzip")
        self.groupBox_4 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_4.setGeometry(QtCore.QRect(470, 230, 411, 121))
        self.groupBox_4.setObjectName("groupBox_4")
        self.editStatus_2 = QtWidgets.QTextEdit(self.groupBox_4)
        self.editStatus_2.setGeometry(QtCore.QRect(20, 20, 361, 91))
        self.editStatus_2.setObjectName("editStatus_2")
        mainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(mainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 903, 21))
        self.menubar.setObjectName("menubar")
        mainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(mainWindow)
        self.statusbar.setObjectName("statusbar")
        mainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(mainWindow)
        QtCore.QMetaObject.connectSlotsByName(mainWindow)

    def retranslateUi(self, mainWindow):
        _translate = QtCore.QCoreApplication.translate
        mainWindow.setWindowTitle(_translate("mainWindow", "OIA Zip Splitter"))
        self.groupBox.setTitle(_translate("mainWindow", "Split Zip File"))
        self.btnChoose.setText(_translate("mainWindow", "Choose File"))
        self.editFileChosen.setText(_translate("mainWindow", "-- Choose a zip file --"))
        self.label.setText(_translate("mainWindow", "Size per file (MB)"))
        self.btnSplit.setText(_translate("mainWindow", "Start Split"))
        self.groupBox_2.setTitle(_translate("mainWindow", "Split Status"))
        self.editStatus.setHtml(_translate("mainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.label_3.setText(_translate("mainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Purpose:<br/></span>To split a large .zip file into multiple smaller .zip files so that they can be<br/>sent via multiple emails (for email with attachment size restriction).</p></body></html>"))
        self.label_4.setText(_translate("mainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">At the receipent end:<br/>- </span>Consolidate all the files into a same folder.<br/>- Notice that only 1 file ends with .zip or .zip.001.<br/>- Right click on the file &gt; Extract here.</p><p><span style=\" font-weight:600;\">Alternatively:</span><br/>Below will generate a complete .zip file and also the unzipped content.</p></body></html>"))
        self.groupBox_3.setTitle(_translate("mainWindow", "Unzip Multipart Zip Files"))
        self.btnChoose_2.setText(_translate("mainWindow", "Choose File"))
        self.editFileChosen_2.setText(_translate("mainWindow", "-- Choose a .zip.001 file --"))
        self.btnUnzip.setText(_translate("mainWindow", "Start Unzip"))
        self.groupBox_4.setTitle(_translate("mainWindow", "Unzip Status"))
        self.editStatus_2.setHtml(_translate("mainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    mainWindow = QtWidgets.QMainWindow()
    ui = Ui_mainWindow()
    ui.setupUi(mainWindow)
    mainWindow.show()
    sys.exit(app.exec_())
