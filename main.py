from PyQt5 import uic, QtCore, QtGui, QtWidgets
import sys
from zipui import Ui_mainWindow
from PyQt5.QtWidgets import QMainWindow, QFileDialog
from PyQt5.QtCore import pyqtSlot
from my_zip import file_split
from my_unzip import extract


class MyWindow(QMainWindow):

    def __init__(self):
        super(MyWindow, self).__init__()

        # set up the UI
        self.ui = Ui_mainWindow()
        self.ui.setupUi(self)
        self.setFixedSize(self.size()) # disable resize

        # For Splitting
        self.ui.btnChoose.clicked.connect(self.choose_split)
        self.ui.btnSplit.clicked.connect(self.start_split)

        # For Unzipping
        self.ui.btnChoose_2.clicked.connect(self.choose_unzip)
        self.ui.btnUnzip.clicked.connect(self.start_unzip)

    # ------------------------------
    # Section 1: For Splitting
    # ------------------------------
    def openFileNameDialog(self):
        # https://pythonspot.com/pyqt5-file-dialog/
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "Zip Files (*.zip*);;Executable Files (*.exe)", options=options)
        if fileName: return fileName

    @pyqtSlot()
    def choose_split(self):
        fileName = self.openFileNameDialog()
        self.ui.editFileChosen.setText(fileName)
        self.ui.btnSplit.setEnabled(True)
        self.ui.editStatus.setText("-")

    @pyqtSlot()
    def start_split(self):
        filename = self.ui.editFileChosen.text()
        self.ui.editStatus.setText("Splitting in progress...")
        filesize_mb = int(self.ui.boxFileSize.text())
        output_folder = file_split(filename, filesize_mb)
        self.ui.editStatus.setText("Successful. Please see: \n    " + output_folder)


    # ------------------------------
    # Section 2: For Unzipping
    # ------------------------------
    def openFileNameDialogUnzip(self):
        # https://pythonspot.com/pyqt5-file-dialog/
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "Multipart Zip Files (*.zip.001)", options=options)
        if fileName: return fileName

    @pyqtSlot()
    def choose_unzip(self):
        fileName = self.openFileNameDialogUnzip()
        self.ui.editFileChosen_2.setText(fileName)
        self.ui.btnUnzip.setEnabled(True)
        self.ui.editStatus_2.setText("-")

    @pyqtSlot()
    def start_unzip(self):
        filename = self.ui.editFileChosen.text()
        self.ui.editStatus_2.setText("Unzipping...")
        full_filename = self.ui.editFileChosen_2.text()
        output_folder = extract(full_filename)
        self.ui.editStatus_2.setText("Successful. Please see: \n    " + output_folder)




if __name__ == "__main__":
    print("\nProgram started...\n")

    app = QtWidgets.QApplication(sys.argv)
    ui = MyWindow()
    ui.show()
    app.exec()

    print("\nBye...")
