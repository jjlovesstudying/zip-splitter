import os



BUF = 10*1024*1024*1024    # 10GB     - memory buffer size

def file_split(file, filesize_mb):
    # Ref: https://stackoverflow.com/questions/20955556/zip-a-folder-into-parts
    max_size = filesize_mb * 1024 * 1024  # 10Mb    - max chapter size
    chapters = 1
    uglybuf = ''

    # e.g. if filename is aaa.zip, output folder will `aaa_out`. Create directory if not available.
    output_directory, short_filename = os.path.split(file)

    output_folder = output_directory + "/" + short_filename[:-4] + "_out"
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    #print("output_folder: ", output_folder)

    with open(file, 'rb') as src:
        while True:
            #tgt = open(file + '.%03d' % chapters, 'wb')
            tgt = open(output_folder + "/" + short_filename + '.%03d' % chapters, 'wb')

            written = 0
            while written < max_size:
                if len(uglybuf) > 0:
                    tgt.write(uglybuf)
                tgt.write(src.read(min(BUF, max_size - written)))
                written += min(BUF, max_size - written)
                uglybuf = src.read(1)
                if len(uglybuf) == 0:
                    break
            tgt.close()
            if len(uglybuf) == 0:
                break
            chapters += 1

    return output_folder